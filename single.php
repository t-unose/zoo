<?php get_header(); ?>
<!-- メインビジュアル -->
<div class="mv">
    <div class="mv-translucent">
        <div class="inner">
            <h1 class="mv-title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<!-- /メインビジュアル -->
<!-- パンくず -->
<div class="inner-breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
</div>
<!-- /パンくず -->
<main class="single-page">
    <!-- 記事ページ -->
    <div class="inner">
        <div class="single-post">
            <div class="single-post__title"><?php the_title(); ?></div>
            <div class="single-post__info">
                <time class="single-post__time"><i class="fas fa-clock"></i><?php the_time('Y.m.d'); ?></time><span class="single-post__category"><i class="fas fa-tag"></i><?php the_category(', '); ?></span><span class="single-post__author"><i class="fas fa-user"></i><?php the_author(); ?></span>
            </div>
            <div class="single-post__thumbnail center"><?php the_post_thumbnail();?></div>
            <div class="single-post__content"><?php the_content(); ?></div>

            <?php // 現在の投稿に隣接している前後の投稿を取得する
            $prev_post = get_previous_post(); // 前の投稿を取得
            $next_post = get_next_post(); // 次の投稿を取得
            if( $prev_post || $next_post ): // どちらか一方があれば表示
            ?>
                <div class="single-post-link">
                    <div class="single-post-link__next">
                    <?php if (get_next_post()): ?>
                        <a class="single-post-link__next-box" href="<?php echo get_permalink( $next_post->ID ); ?>"><?php echo get_the_post_thumbnail($next_post->ID,array( 'class' => 'single-post-link__img' )); ?><p class="single-post-link__title"><?php echo $next_post->post_title ?></p></a>
                    <?php endif; ?>
                    </div>
                    <div class="single-post-link__prev">
                    <?php if (get_previous_post()):?>
                    <?php $next_post= $next_post->ID; ?>
                        <a class="single-post-link__prev-box" href="<?php echo get_permalink( $prev_post->ID ); ?>"><?php echo get_the_post_thumbnail($prev_post->ID,array( 'class' => 'single-post-link__img' )); ?><p class="single-post-link__title"><?php echo $prev_post->post_title ?></p></a>
                    <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    
    <div class="">
        <div class="inner">
            <h2>関連記事</h2>
            <!-- related-list -->
            <div class="related-list">
                <div class="related-list__display owl-carousel front-view-content">
                    <?php
                        // カテゴリーが複数設定されている場合は、どれかをランダムに取得
                        $categories = wp_get_post_categories($post->ID, array('orderby'=>'rand'));
                        //表示したい記事要素を設定
                        if ($categories) {
                            $args = array(
                                'category__in' => array($categories[0]), // カテゴリーのIDで記事を取得
                                'post__not_in' => array($post->ID), // 表示している記事は除外する
                                'showposts'=>12, // 取得したい記事数
                                'caller_get_posts'=>1, // 取得した記事を1番目から表示する
                                'orderby'=> 'rand' // ランダムで取得する
                            ); 
                        $my_query = new WP_Query($args);
                        if( $my_query->have_posts() ) {
                        while ($my_query->have_posts()) { $my_query->the_post();
                        ?>
                            <div class="related-article">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <div class="related-article__block">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <?php the_post_thumbnail('thumbnail'); ?>
                                    <?php else : ?>
                                        <img class="related-article__thumbnail" src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="デフォルト画像" />
                                    <?php endif ; ?>
                                        <h3 class="related-article__title"><?php the_title(); ?></h3>
                                        <p class="related-article__time"><?php
                                            $category = get_the_category();
                                            $cat_name = $category[0]->cat_name;echo $cat_name; ?> | <?php the_time('Y.m.d'); ?></p>
                                    </div>
                                </a>
                            </div>
                    <?php } wp_reset_query();
                    } else { ?>
                        <p class="">関連記事がない</p>
                <?php } } ?>
                </div>
            </div>
            <!-- /related-list -->
        </div>
    </div>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
