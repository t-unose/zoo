<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title(); ?></title>
        <!-- -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/OwlCarousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/OwlCarousel/owl.theme.default.css">
    <!-- -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/cssreset-min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>

    <script>
    $(function(){
        // #で始まるリンクをクリックしたら実行されます
        $('a[href^="#"]').click(function() {
            var speed = 600; // ミリ秒で記述
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top;
            $('body,html').animate({scrollTop:position}, speed, 'swing');
            return false;
        });
    });
    </script>

    <!-- ハンバーガーメニュー -->
    <script>
        $(function(){
    $('.header-sp__menu-trigger').on('click', function() {
        $('header').toggleClass('active');
        return false;
    });
    });
    </script>
    <!-- /ハンバーガーメニュー -->

    <?php wp_deregister_script('jquery'); ?>
    <?php wp_head(); ?>
</head>
<body id="body" class="body">
    
    <!-- header -->
    <header class="header">
        <!-- header-sp -->
            <div class="header-sp">
                <div class="header-sp__title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo(); ?></a></div>
                <div class="header-sp__menu"><a class="header-sp__menu-trigger" href="">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a></div>
            </div>
            <!-- /header-sp -->
        <div class="header-box">
            <div class="header-logo center">
                <a href="<?php bloginfo('url') ?>" class=""><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>" class=""></a>
                <p><?php bloginfo( 'description' ); ?></p>
            </div>
            <?php wp_nav_menu( array(
                'theme_location'    =>'header_menu',
                'container'         =>'nav',
                'container_class'   =>'header-menu',
                'menu_class'        =>'header-menu__list',
                'depth'             =>'0',
                'items_wrap'        =>'<ul class="%2$s">%3$s</ul>'));
            ?>
            <div class="header-social">
                <ul class="header-social__list">
                <?php if(get_theme_mod( 'info__facebook')): ?>
                    <li class="header-social__item"><a href="<?php echo get_theme_mod( 'info__facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/header-social__item1.svg" alt="socialボタン"></a></li>
                <?php endif; ?>
                <?php if(get_theme_mod( 'info__twitter')): ?>
                    <li class="header-social__item"><a href="<?php echo get_theme_mod( 'info__twitter'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/header-social__item2.svg" alt="socialボタン"></a></li>
                <?php endif; ?>
                <?php if(get_theme_mod( 'info__instagram')): ?>
                    <li class="header-social__item"><a href="<?php echo get_theme_mod( 'info__instagram'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/header-social__item3.svg" alt="socialボタン"></a></li>
                <?php endif; ?>
                </ul>
            </div>
            <div class="header-info center">
                <h1 class="header-info__name"><?php echo get_theme_mod( 'info__name'); ?></h1>
                <p class="header-info__address"><?php echo get_theme_mod( 'info__yuubin'); ?><br><?php echo get_theme_mod( 'info__address'); ?></p>
                <p class="header-info__tel">TEL:<a href="tel:<?php echo get_theme_mod( 'info__tel'); ?>"><?php echo get_theme_mod( 'info__tel'); ?></a></p>
            </div> 
        </div>
    </header>
    <!-- /header -->
    <!-- メインカラム -->
    <div class="main-column">