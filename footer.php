        <!--
            footer-sitemaps
        -->
        <?php if ( !is_home() && !is_front_page() ) : ?>
        <div class="">
            <div class="inner">
                <div class="center">
                    <a href="<?php bloginfo('url') ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>">
                    </a>
                </div>
                <div class="footer-sitemaps">
                <?php if( has_nav_menu('footer-sitemaps_menu1') ) : ?>
                <div class="footer-menu footer-menu1">
                    <h3 class="footer-menu__title">アフリカサバンナゾーン</h3>
                    <?php wp_nav_menu( array(
                            'theme_location'    =>'footer-sitemaps_menu1',
                            'container'         =>'div',
                            'container_class'   =>'',
                            'menu_class'        =>'footer-menu__list footer-menu1__list',
                            'items_wrap'         =>'<ul class="%2$s">%3$s</ul>'));
                    ?>
                </div>
                <?php endif; ?>
                <?php if( has_nav_menu('footer-sitemaps_menu2') ) : ?>
                <div class="footer-menu footer-menu2">
                    <h3 class="footer-menu__title">コアラ・サル エリア</h3>
                    <?php wp_nav_menu( array(
                            'theme_location'    =>'footer-sitemaps_menu2',
                            'container'         =>'div',
                            'container_class'   =>'',
                            'menu_class'        =>'footer-menu__list footer-menu2__list',
                            'items_wrap'        =>'<ul class="%2$s">%3$s</ul>'));
                    ?>
                </div>
                <?php endif; ?>
                <?php if( has_nav_menu('footer-sitemaps_menu3') ) : ?>
                <div class="footer-menu footer-menu3">
                    <h3 class="footer-menu__title">ふれあい広場</h3>
                    <?php wp_nav_menu( array(
                            'theme_location'    =>'footer-sitemaps_menu3',
                            'container'         =>'div',
                            'container_class'   =>'',
                            'menu_class'        =>'footer-menu__list footer-menu3__list',
                            'items_wrap'        =>'<ul class="%2$s">%3$s</ul>'));
                    ?>
                </div>
                <?php endif; ?>
                <?php if( has_nav_menu('footer-sitemaps_menu4') ) : ?>
                    <div class="footer-menu footer-menu4">
                        <h3 class="footer-menu__title">鳥の楽園と周辺エリア</h3>
                        <?php wp_nav_menu( array(
                                'theme_location'    =>'footer-sitemaps_menu4',
                                'container'         =>'div',
                                'container_class'   =>'',
                                'menu_class'        =>'footer-menu__list footer-menu4__list',
                                'items_wrap'        =>'<ul class="%2$s">%3$s</ul>'));
                        ?>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
        <?php else: ?>
        <?php endif; ?>
        <!-- /footer-sitemaps -->
        <!-- footer -->
        <footer class="footer">
            <div class="footer-info">
                <div clas="inner">
                        <p>
                        <?php echo get_theme_mod( 'info__name'); ?> <?php echo get_theme_mod( 'info__address'); ?> TEL:<a href="tel:<?php echo get_theme_mod( 'info__tel'); ?>"><?php echo get_theme_mod( 'info__tel'); ?></a>
                        </p>
                </div>
            </div>
            <div class="footer-copyright">
                <div clas="inner">© 2019 <?php echo get_theme_mod( 'info__name'); ?>.</div>
            </div>
        </footer>
        <div clas="page-top">
            <a href=""></a>
        </div>
        <!-- /footer -->
        </div>
        <!-- /メインカラム -->
        <script src="https://code.jquery.com/jquery-latest.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')
  </script>
<script src="<?php bloginfo('template_directory'); ?>/js/OwlCarousel/owl.carousel.js"></script>
<?php if(count($posts)>1): ?>
<script>
    function ticker(){
        $('#news li:first').slideUp( function(){
            $(this).appendTo($('#news')).slideDown();
        });
    }
    setInterval(function(){ticker()}, 5000);
    </script>
<?php endif; ?>
<script>
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            stagePadding: 25,
            loop:true,
            margin:10,
            nav:true,
            autoplay:true,
            dots:false,
            responsive:{
                0:{
                    items:2
                },
                700:{
                    items:3
                },
            }
        })
    });
</script>
<!-- IEカクツキ修正 -->
<script>
    if(navigator.userAgent.match(/MSIE 10/i) || navigator.userAgent.match(/Trident\/7\./) || navigator.userAgent.match(/Edge\/12\./)) {
    $('body').on("mousewheel", function () {
        event.preventDefault();
        var wd = event.wheelDelta;
        var csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
    });
    }
</script>
<!-- /IEカクツキ修正 -->
<!-- フェードイン -->
<script>
$(function(){
    $(window).scroll(function (){
        $('.fadeIn').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 200){
                $(this).addClass('scrollin');
            }
        });
    });
});
</script>
<script>
$(function(){
    $(window).load(function (){
        $('.fadeIn').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 200){
                $(this).addClass('scrollin');
            }
        });
    });
});
</script>
<!-- /フェードイン -->
    <?php wp_footer(); ?>
    </body>
</html>