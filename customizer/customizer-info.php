<?php
    //info1
    $wp_customize->add_section( 'info_settings', array (
        'title'  => __( '店舗情報'),
        'priority' => 36,
    ) );
        //店舗情報 店舗名
        $wp_customize->add_setting( 'info__name', array(
        'default' => null,
        ));
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__name', array(
        'label' => '店舗名',
        'section' => 'info_settings',
        'settings' => 'info__name',
        'priority' => 1,
        )));
        //店舗情報 郵便番号
        $wp_customize->add_setting( 'info__yuubin', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__yuubin', array(
            'label' => '郵便番号',
            'section' => 'info_settings',
            'settings' => 'info__yuubin',
            'priority' => 2,
            )));
        //店舗情報 住所
        $wp_customize->add_setting( 'info__address', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__address', array(
            'label' => '住所',
            'section' => 'info_settings',
            'settings' => 'info__address',
            'priority' => 3,
            )));
        //店舗情報 電話番号
        $wp_customize->add_setting( 'info__tel', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__tel', array(
            'label' => '電話番号',
            'section' => 'info_settings',
            'settings' => 'info__tel',
            'priority' => 4,
            )));
        //店舗情報 メールアドレス
        $wp_customize->add_setting( 'info__email', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__email', array(
            'label' => 'メールアドレス',
            'section' => 'info_settings',
            'settings' => 'info__email',
            'priority' => 5,
            )));
        //店舗情報 Twitter
        $wp_customize->add_setting( 'info__twitter', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__twitter', array(
            'label' => 'Twitter URL',
            'section' => 'info_settings',
            'settings' => 'info__twitter',
            'priority' => 6,
            )));
        //店舗情報 Facebook
        $wp_customize->add_setting( 'info__facebook', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__facebook', array(
            'label' => 'Facebook URL',
            'section' => 'info_settings',
            'settings' => 'info__facebook',
            'priority' => 7,
            )));
        //店舗情報 Instagram
        $wp_customize->add_setting( 'info__instagram', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'info__instagram', array(
            'label' => 'Instagram URL',
            'section' => 'info_settings',
            'settings' => 'info__instagram',
            'priority' => 8,
            )));