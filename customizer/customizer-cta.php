<?php
$wp_customize->add_panel( 'cta_settings', array(
    'priority' => 35,
    'capability' => 'edit_theme_options',
    'title' => __( 'CTA設定'),
    ) );

    //CTA1
    $wp_customize->add_section( 'cta-1_settings', array (
        'title'  => __( 'CTA1'),
        'priority' => 1,
        'panel' => 'cta_settings',
    ) );
        //CTA1 表示
        $wp_customize->add_setting( 'cta1__view', array(
        'default'   => false,
        ));
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta1__view', array(
        'label' => 'CTA1を表示',
        'section' => 'cta-1_settings',
        'settings' => 'cta1__view',
        'priority' => 1,
        'type'      => 'checkbox',
        )));
        //CTA1 見出し
        $wp_customize->add_setting( 'cta1__title', array(
        'default' => null,
        ));
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta1__title', array(
        'label' => '見出し',
        'section' => 'cta-1_settings',
        'settings' => 'cta1__title',
        'priority' => 2,
        )));
        //CTA1 テキスト
        $wp_customize->add_setting( 'cta1__text', array(
        'default' => null,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta1__text', array(
        'label' => 'テキスト',
        'section' => 'cta-1_settings',
        'settings' => 'cta1__text',
        'priority' => 3,
        'type'      => 'textarea',
        )));
        //CTA1 画像
        $wp_customize->add_setting( 'cta1__img', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta1__img', array(
            'label' => '画像',
            'section' => 'cta-1_settings',
            'settings' => 'cta1__img',
            'priority' => 4,
            )));
        //CTA1 リンクテキスト
        $wp_customize->add_setting( 'cta1__link-text', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta1__link-text', array(
            'label' => 'リンクテキスト',
            'section' => 'cta-1_settings',
            'settings' => 'cta1__link-text',
            'priority' => 5,
            )));
        //CTA1 リンクURL
        $wp_customize->add_setting( 'cta1__link-url', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta1__link-url', array(
            'label' => 'リンクURL',
            'section' => 'cta-1_settings',
            'settings' => 'cta1__link-url',
            'priority' => 6,
            )));
    //CTA2
    $wp_customize->add_section( 'cta-2_settings', array (
        'title'  => __( 'CTA2'),
        'priority' => 2,
        'panel' => 'cta_settings',
    ) );
        //CTA2 表示
        $wp_customize->add_setting( 'cta2__view', array(
            'default'   => false,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta2__view', array(
            'label' => 'CTA2を表示',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__view',
            'priority' => 1,
            'type'      => 'checkbox',
            )));
        //CTA2 見出し
        $wp_customize->add_setting( 'cta2__title', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta2__title', array(
            'label' => '見出し',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__title',
            'priority' => 2,
        )));
        //CTA2 テキスト
        $wp_customize->add_setting( 'cta2__text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta2__text', array(
            'label' => 'テキスト',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__text',
            'priority' => 3,
            'type'      => 'textarea',
        )));
        //CTA2 画像
        $wp_customize->add_setting( 'cta2__img', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta2__img', array(
            'label' => '画像',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__img',
            'priority' => 4,
        )));
        //CTA2 リンクテキスト
        $wp_customize->add_setting( 'cta2__link-text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta2__link-text', array(
            'label' => 'リンクテキスト',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__link-text',
            'priority' => 5,
        )));
        //CTA2 リンクURL
        $wp_customize->add_setting( 'cta2__link-url', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta2__link-url', array(
            'label' => 'リンクURL',
            'section' => 'cta-2_settings',
            'settings' => 'cta2__link-url',
            'priority' => 6,
        )));


    //CTA3
    $wp_customize->add_section( 'cta-3_settings', array (
        'title'  => __( 'CTA3'),
        'priority' => 3,
        'panel' => 'cta_settings',
    ) );
        //CTA3 表示
        $wp_customize->add_setting( 'cta3__view', array(
            'default'   => false,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta3__view', array(
            'label' => 'CTA3を表示',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__view',
            'priority' => 1,
            'type'      => 'checkbox',
            )));
        //CTA3 見出し
        $wp_customize->add_setting( 'cta3__title', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta3__title', array(
            'label' => '見出し',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__title',
            'priority' => 2,
        )));
        //CTA3 テキスト
        $wp_customize->add_setting( 'cta3__text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta3__text', array(
            'label' => 'テキスト',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__text',
            'priority' => 3,
            'type'      => 'textarea',
        )));
        //CTA3 画像
        $wp_customize->add_setting( 'cta3__img', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta3__img', array(
            'label' => '画像',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__img',
            'priority' => 4,
        )));
        //CTA3 リンクテキスト
        $wp_customize->add_setting( 'cta3__link-text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta3__link-text', array(
            'label' => 'リンクテキスト',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__link-text',
            'priority' => 5,
        )));
        //CTA3 リンクURL
        $wp_customize->add_setting( 'cta3__link-url', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta3__link-url', array(
            'label' => 'リンクURL',
            'section' => 'cta-3_settings',
            'settings' => 'cta3__link-url',
            'priority' => 6,
        )));

    //CTA4
    $wp_customize->add_section( 'cta-4_settings', array (
        'title'  => __( 'CTA4'),
        'priority' => 4,
        'panel' => 'cta_settings',
    ) );
        //CTA1 表示
        $wp_customize->add_setting( 'cta4__view', array(
            'default'   => false,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta4__view', array(
            'label' => 'CTA4を表示',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__view',
            'priority' => 1,
            'type'      => 'checkbox',
            )));
        //CTA4 見出し
        $wp_customize->add_setting( 'cta4__title', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta4__title', array(
            'label' => '見出し',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__title',
            'priority' => 2,
        )));
        //CTA4 テキスト
        $wp_customize->add_setting( 'cta4__text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta4__text', array(
            'label' => 'テキスト',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__text',
            'priority' => 3,
            'type'      => 'textarea',
        )));
        //CTA4 画像
        $wp_customize->add_setting( 'cta4__img', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta4__img', array(
            'label' => '画像',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__img',
            'priority' => 4,
        )));
        //CTA4 リンクテキスト
        $wp_customize->add_setting( 'cta4__link-text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta4__link-text', array(
            'label' => 'リンクテキスト',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__link-text',
            'priority' => 5,
        )));
        //CTA4 リンクURL
        $wp_customize->add_setting( 'cta4__link-url', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta4__link-url', array(
            'label' => 'リンクURL',
            'section' => 'cta-4_settings',
            'settings' => 'cta4__link-url',
            'priority' => 6,
        )));

    //CTA5
    $wp_customize->add_section( 'cta-5_settings', array (
        'title'  => __( 'CTA5'),
        'priority' => 5,
        'panel' => 'cta_settings',
    ) );
        //CTA1 表示
        $wp_customize->add_setting( 'cta5__view', array(
            'default'  => false,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta5__view', array(
            'label' => 'CTA5を表示',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__view',
            'priority' => 1,
            'type'      => 'checkbox',
            )));
        //CTA5 見出し
        $wp_customize->add_setting( 'cta5__title', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta5__title', array(
            'label' => '見出し',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__title',
            'priority' => 2,
        )));
        //CTA5 テキスト
        $wp_customize->add_setting( 'cta5__text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta5__text', array(
            'label' => 'テキスト',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__text',
            'priority' => 3,
            'type'     => 'textarea',
        )));
        //CTA5 画像
        $wp_customize->add_setting( 'cta5__img', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'cta5__img', array(
            'label' => '画像',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__img',
            'priority' => 4,
        )));
        //CTA5 リンクテキスト
        $wp_customize->add_setting( 'cta5__link-text', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta5__link-text', array(
            'label' => 'リンクテキスト',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__link-text',
            'priority' => 5,
        )));
        //CTA5 リンクURL
        $wp_customize->add_setting( 'cta5__link-url', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'cta5__link-url', array(
            'label' => 'リンクURL',
            'section' => 'cta-5_settings',
            'settings' => 'cta5__link-url',
            'priority' => 6,
        )));