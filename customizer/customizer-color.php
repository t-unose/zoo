<?php
    $wp_customize->add_panel( 'color_settings', array(
        'priority' => 30,
        'capability' => 'edit_theme_options',
        'title' => __( '文字色と背景色'),
        ) );

    //header
        $wp_customize->add_section( 'color-header_settings', array (
            'title'  => __( 'ヘッダー'),
            'priority' => 1,
            'panel' => 'color_settings',
        ) );
            //ヘッダー 背景色
            $wp_customize->add_setting( 'header__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header__background-color', array(
            'label' => '背景色',
            'section' => 'color-header_settings',
            'settings' => 'header__background-color',
            'priority' => 20,
            )));
            //ヘッダー 文字色
            $wp_customize->add_setting( 'header__font-color', array(
                'default' => null,
                ) );
                $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header__font-color', array(
                'label' => '文字色',
                'section' => 'color-header_settings',
                'settings' => 'header__font-color',
                'priority' => 20,
                )));
    //body
    $wp_customize->add_section( 'color-body_settings', array (
        'title'  => __( 'ボディ'),
        'priority' => 2,
        'panel' => 'color_settings',
    ) );
        //ボディ 文字色
        $wp_customize->add_setting( 'body__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body__font-color', array(
            'label' => '文字色',
            'section' => 'color-body_settings',
            'settings' => 'body__font-color',
            'priority' => 20,
            )));
        //ボディ 背景色
        $wp_customize->add_setting( 'body__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body__background-color', array(
            'label' => '背景色',
            'section' => 'color-body_settings',
            'settings' => 'body__background-color',
            'priority' => 20,
            )));
    //footer
    $wp_customize->add_section( 'color-footer_settings', array (
        'title'  => __( 'フッター'),
        'priority' => 3,
        'panel' => 'color_settings',
    ) );
        //フッターコピーライト 文字色
        $wp_customize->add_setting( 'footer-copyright__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer-copyright__font-color', array(
            'label' => 'コピーライト 文字色',
            'section' => 'color-footer_settings',
            'settings' => 'footer-copyright__font-color',
            'priority' => 20,
            )));
        //コピーライト 背景色
        $wp_customize->add_setting( 'footer-copyright__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer-copyright__background-color', array(
            'label' => 'コピーライト背景色',
            'section' => 'color-footer_settings',
            'settings' => 'footer-copyright__background-color',
            'priority' => 20,
            )));
        //店舗情報 文字色
        $wp_customize->add_setting( 'footer-info__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer-info__font-color', array(
            'label' => '店舗情報 文字色',
            'section' => 'color-footer_settings',
            'settings' => 'footer-info__font-color',
            'priority' => 20,
            )));
        //店舗情報  背景色
        $wp_customize->add_setting( 'footer-info__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer-info__background-color', array(
            'label' => '店舗情報  背景色',
            'section' => 'color-footer_settings',
            'settings' => 'footer-info__background-color',
            'priority' => 20,
            )));
    //メインビジュアル
    $wp_customize->add_section( 'color-mv_settings', array (
        'title'  => __( 'メインビジュアル'),
        'priority' => 4,
        'panel' => 'color_settings',
    ) );
        //メインビジュアル 文字色
        $wp_customize->add_setting( 'mv__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mv__font-color', array(
            'label' => 'メインビジュアル 文字色',
            'section' => 'color-mv_settings',
            'settings' => 'mv__font-color',
            'priority' => 20,
            )));
    //その他
    $wp_customize->add_section( 'color-other_settings', array (
        'title'  => __( 'その他'),
        'priority' => 4,
        'panel' => 'color_settings',
    ) );
        //ページネーション 選択ページ 文字色
        $wp_customize->add_setting( 'page-numbers-naw__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page-numbers-naw__font-color', array(
            'label' => 'ページネーション 選択ページ 文字色',
            'section' => 'color-other_settings',
            'settings' => 'page-numbers-naw__font-color',
            'priority' => 20,
            )));
        //ページネーション 選択ページ 背景色
        $wp_customize->add_setting( 'page-numbers-naw__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page-numbers-naw__background-color', array(
            'label' => 'ページネーション 選択ページ 背景色',
            'section' => 'color-other_settings',
            'settings' => 'page-numbers-naw__background-color',
            'priority' => 20,
            )));
        //ページネーション その他ページ 文字色
        $wp_customize->add_setting( 'page-numbers__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page-numbers__font-color', array(
            'label' => 'ページネーション その他ページ 文字色',
            'section' => 'color-other_settings',
            'settings' => 'page-numbers__font-color',
            'priority' => 20,
            )));
        //ページネーション その他ページ 背景色
        $wp_customize->add_setting( 'page-numbers__background-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page-numbers__background-color', array(
            'label' => 'ページネーション その他ページ 背景色',
            'section' => 'color-other_settings',
            'settings' => 'page-numbers__background-color',
            'priority' => 20,
            )));
        //パンくずリスト 文字色
        $wp_customize->add_setting( 'breadcrumbs__font-color', array(
            'default' => null,
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumbs__font-color', array(
            'label' => 'パンくずリスト 文字色',
            'section' => 'color-other_settings',
            'settings' => 'breadcrumbs__font-color',
            'priority' => 20,
            )));