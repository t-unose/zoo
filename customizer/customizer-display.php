<?php
    //display1
    $wp_customize->add_section( 'display_settings', array (
        'title'  => __( '表示設定'),
        'priority' => 21,
    ) );
        //表示設定 ニュース表示件数
        $wp_customize->add_setting( 'display__news', array(
        'default' => 5,
        ));
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'display__news', array(
        'label' => 'トップページ ニュース表示件数',
        'section' => 'display_settings',
        'settings' => 'display__news',
        'priority' => 1,
        )));
        //表示設定 デフォルトアイキャッチ
        $wp_customize->add_setting( 'display__thumbnail', array(
            'default' => null,
            ));
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'display__thumbnail', array(
            'label' => 'デフォルトアイキャッチ',
            'section' => 'display_settings',
            'settings' => 'display__thumbnail',
            'priority' => 2,
            )));
            