# 仕様書


## 対応ブラウザ
### PC
- safari
- Chrome
- Edge
- IE11


### スマホ
- 初期インストールブラウザ

---
## 開発環境

### 開発環境
- LOCAL byFlywheel
- SCSS（CSSプリプロセッサ）

### ワードプレス
- All-in-One WP Migration
- Show Current Template



---
## 命名規則
BEMを使用

    Block / 構成のルートとなる親要素。
    Element / Blockに付随する子要素。
    Modifier / 状態変化を表す要素。

    block__element--modifier のように, blockとelementはアンダースコア2つで区切り, elementとmodifierはハイフン2つで区切る。
    blockが複数単語になる場合は, 単語と単語の間はハイフン1つで区切る. element, modifierが複数単語になる場合も同様である。


---
## サイトマップ
    TOP
    ├ NEWS
    │ └ single-NESW
    ├ ABOUT
    ├ MENU
    ├ ACCESS
    ├ BLOG
    │ ├ category-BLOG
    │ └ single-BLOG
    ├ MAP
    └ CONTACT

---
## 作成ファイル

| 作成ファイル名 | 用途 | 使用ページ |
|:-----------|------------:|:------------:|
| index.php | ベース | ベース |
| style.scss | style.css書き出し用 | 共通 |
| style.css | デザイン | 共通 |
| function.php |  | 共通 |
| header.php | ヘッター | 共通 |
| footer.php | フッター | 共通 |
| sidebar.php | サイドバー | 必要なし？ |
| 404.php | 404ページ | 404 |
| home.php | トップページ | TOP |
| page.php | 固定ページ | ABOUT,MENU,ACCESS,MAP,CONTACT |
| archive.php | アーカイブページ | NEWS,BLOG |
| single.php | 個別ページ | single-NESW,single-BLOG |

---
## 利用プラグイン
### ワードプレス
- Yoast  SEO（パンくず表示）
- MW WP Form（コンタクト）
- Adminimize（ユーザー権限管理）
- Custom Post Type UI（カスタム投稿タイプ）

### jQuery
- 
- 
- 
- 

### その他
- 
- 
- 
- 