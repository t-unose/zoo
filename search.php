<?php get_header(); ?>
<!-- メインビジュアル -->
<div class="mv">
    <div class="mv-translucent">
        <div class="inner">
                <h1 class="mv-title"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
<!-- /メインビジュアル -->
<!-- パンくず -->
<div class="inner-breadcrumbs">
    <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
</div>
<!-- /パンくず -->
<main>
    <!-- アーカイブ -->
    <div class="inner">
        <div class="article-list">
            <?php if ( have_posts() ) {
                while ( have_posts() ){
                    the_post();
                    ?>
                    <article class="article-list-item">
                        <a class="article-list-item__link" href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            <?php else : ?>
                                <img class="article-list-item__thumbnail" src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="デフォルト画像" />
                            <?php endif ; ?>
                            <h2 class="article-list-item__title"><?php the_title(); ?></h2>
                            <div class="article-list-item__info">
                                <p class="">
                                    <span class="article-list-item__category"><?php
                                        $category = get_the_category();
                                        $cat_name = $category[0]->cat_name;echo $cat_name; ?></span> | <span class="article-list-item__time"><?php the_time('Y.m.d'); ?></span></p>
                            </div>
                        </a>
                    </article>
                    <?php
                }
            }
            ?>
        </div>
        <?php
            //ページネーション実装
            $big = 9999999999;
            $arg = array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'current' => max( 1, get_query_var('paged') ),
                'total'   => $wp_query->max_num_pages,
                'prev_text'    => '<i class="fas fa-angle-double-left"></i>前へ',
                'next_text'    => '次へ<i class="fas fa-angle-double-right"></i>',
                'type'    => 'list'
            );
            echo paginate_links($arg);
        ?>
    </div>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>