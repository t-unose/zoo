<?php
//カスタマイザー
function theme_customizer_extension($wp_customize) {

    // color Panel 
    require get_template_directory() . '/customizer/customizer-color.php';

    // cta Panel 
    require get_template_directory() . '/customizer/customizer-cta.php';

    // info Panel 
    require get_template_directory() . '/customizer/customizer-info.php';

    // display Panel 
    require get_template_directory() . '/customizer/customizer-display.php';

}



//カスタマイザーの内容を反映
add_action('customize_register', 'theme_customizer_extension');
    function customizer_option() {
       echo '<style type="text/css">';
    ?>
    <?php //CTA1背景
    if ( get_theme_mod( 'cta1__img') ) {?>
            .top-mv__background--1 {
            background-image: url(<?php echo get_theme_mod( 'cta1__img', '#FFFFFF'); ?>);
            }
    <?php } ?>
    <?php //CTA2背景
    if ( get_theme_mod( 'cta2__img') ) {?>
            .top-mv__background--2 {
            background-image: url(<?php echo get_theme_mod( 'cta2__img', '#FFFFFF'); ?>);
            }
    <?php } ?>
    <?php //CTA3背景
    if ( get_theme_mod( 'cta3__img') ) {?>
            .top-mv__background--3 {
            background-image: url(<?php echo get_theme_mod( 'cta3__img', '#FFFFFF'); ?>);
            }
    <?php } ?>
    <?php //CTA4背景
    if ( get_theme_mod( 'cta4__img') ) {?>
            .top-mv__background--4 {
            background-image: url(<?php echo get_theme_mod( 'cta4__img', '#FFFFFF'); ?>);
            }
    <?php } ?>
    <?php //CTA5背景
    if ( get_theme_mod( 'cta5__img') ) {?>
            .top-mv__background--5 {
            background-image: url(<?php echo get_theme_mod( 'cta5__img', '#FFFFFF'); ?>);
            }
    <?php } ?>

    <?php //ヘッダー背景色
    if ( get_theme_mod( 'header__background-color') ) {?>
            .header {
            background-color: <?php echo get_theme_mod( 'header__background-color', '#FFFFFF'); ?>;
            }
    <?php } ?>
    <?php //ヘッダーメニュー文字色
    if ( get_theme_mod( 'header__font-color') ) {?>
            .header-menu__list {
            color: <?php echo get_theme_mod( 'header__font-color', '#FFFFFF'); ?>;
            }
    <?php } ?>

    <?php //ボディ文字色
    if ( get_theme_mod( 'body__font-color') ) {?>
        body{
            color: <?php echo get_theme_mod( 'body__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //ボディ背景色
    if ( get_theme_mod( 'body__background-color') ) {?>
        body{
            background-color: <?php echo get_theme_mod( 'body__background-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //フッターコピーライト 文字色
    if ( get_theme_mod( 'footer-copyright__font-color') ) {?>
        .footer-copyright{
            color: <?php echo get_theme_mod( 'footer-copyright__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //コピーライト 背景色
    if ( get_theme_mod( 'footer-copyright__background-color') ) {?>
        .footer-copyright{
            background-color: <?php echo get_theme_mod( 'footer-copyright__background-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //店舗情報 文字色
    if ( get_theme_mod( 'footer-info__font-color') ) {?>
        .footer-info{
            color: <?php echo get_theme_mod( 'footer-info__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //店舗情報  背景色
    if ( get_theme_mod( 'footer-info__background-color') ) {?>
        .footer-info{
            background-color: <?php echo get_theme_mod( 'footer-info__background-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //メインビジュアル 文字色
    if ( get_theme_mod( 'mv__font-color') ) {?>
        .mv-title{
            color: <?php echo get_theme_mod( 'mv__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //ページネーション 選択ページ 文字色
    if ( get_theme_mod( 'page-numbers-naw__font-color') ) {?>
        .page-numbers.current{
            color: <?php echo get_theme_mod( 'page-numbers-naw__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //ページネーション 選択ページ 背景色
    if ( get_theme_mod( 'page-numbers-naw__background-color') ) {?>
        .page-numbers.current{
            background-color: <?php echo get_theme_mod( 'page-numbers-naw__background-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //ページネーション その他ページ 文字色
    if ( get_theme_mod( 'page-numbers__font-color') ) {?>
        .page-numbers{
            color: <?php echo get_theme_mod( 'page-numbers__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //ページネーション その他ページ 背景色
    if ( get_theme_mod( 'page-numbers__background-color') ) {?>
        .page-numbers{
            background-color: <?php echo get_theme_mod( 'page-numbers__background-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php //パンくずリスト 文字色
    if ( get_theme_mod( 'breadcrumbs__font-color') ) {?>
        #breadcrumbs{
            color: <?php echo get_theme_mod( 'breadcrumbs__font-color', '#FFFFFF'); ?>;
        }
    <?php } ?>

    <?php
        echo '</style>';
    }
add_action( 'wp_head', 'customizer_option');
