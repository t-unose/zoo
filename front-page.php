
<?php get_header(); ?>
<?php
$posts = get_posts('post_type=news&posts_per_page='.get_theme_mod( 'display__news')); 
?>
        
<!-- メインビジュアル -->
<div class="news-bar">
    <div class="news-bar-container1">
        <?php if (!empty($posts)): ?>
        <ul lass="news-bar-container1__list ticker" id="news">
        <?php foreach ($posts as $post):setup_postdata($post); ?>
        <li class="news-bar-container1__item"><a href="<?php the_permalink(); ?>"><?php the_time('Y.m.d'); ?> <?php the_title(); ?></a></li>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
        </ul>
        <?php endif; ?>
        
    </div>
    <div class="news-bar-container2">
        <a href="<?php  home_url(); ?>/news">NEWS一覧　<i class="fas fa-chevron-right"></i></a>
    </div>
</div>
<?php if(get_theme_mod( 'cta1__view')): ?>
    <div class="top-mv top-mv__background--1" id="top1">
        <div class="inner">
            <div class="top-mv-container fadeIn">
                <h1 class="top-mv-container__title"><?php echo get_theme_mod( 'cta1__title');?></h1>
                <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta1__text');?></p>
                <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta1__link-url');?>"><?php echo get_theme_mod( 'cta1__link-text');?></a>
            </div>
            <div class="top-mv-controller">
                <div class="top-mv-controller__down"><a href="<?php for($i = 2; $i <= 5; $i++){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }
                    if($i <= 5){
                        echo '#top6';
                    }} ?>"><i class="fas fa-chevron-down"></i></a></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(get_theme_mod( 'cta2__view')): ?>
    <div class="top-mv top-mv__background--2" id="top2">
        <div class="inner">
            <div class="top-mv-container fadeIn">
                <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta2__title');?></h2>
                <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta2__text');?></p>
                <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta2__link-url');?>"><?php echo get_theme_mod( 'cta2__link-text');?></a>
            </div>
            <div class="top-mv-controller">
                <div class="top-mv-controller__down"><a href="<?php for($i = 3; $i <= 5; $i++){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }
                    if($i <= 5){
                        echo '#top6';
                    }} ?>"><i class="fas fa-chevron-down"></i></a></div>
                <div class="top-mv-controller__up"><a href="<?php for($i = 1; $i >= 1; $i--){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }} ?>"><i class="fas fa-chevron-up"></i></a></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(get_theme_mod( 'cta3__view')): ?>
    <div class="top-mv top-mv__background--3" id="top3">
        <div class="inner">
            <div class="top-mv-container fadeIn">
                <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta3__title');?></h2>
                <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta3__text');?></p>
                <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta3__link-url');?>"><?php echo get_theme_mod( 'cta3__link-text');?></a>
            </div>
            <div class="top-mv-controller">
                <div class="top-mv-controller__down"><a href="<?php for($i = 4; $i <= 5; $i++){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }
                    if($i <= 5){
                        echo '#top6';
                    }} ?>"><i class="fas fa-chevron-down"></i></a></div>
                <div class="top-mv-controller__up"><a href="<?php for($i = 2; $i >= 1; $i--){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }} ?>"><i class="fas fa-chevron-up"></i></a></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(get_theme_mod( 'cta4__view')): ?>
    <div class="top-mv top-mv__background--4" id="top4">
        <div class="inner">
            <div class="top-mv-container fadeIn">
                <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta4__title');?></h2>
                <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta4__text');?></p>
                <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta4__link-url');?>"><?php echo get_theme_mod( 'cta4__link-text');?></a>
            </div>
            <div class="top-mv-controller">
                <div class="top-mv-controller__down"><a href="<?php for($i = 5; $i <= 5; $i++){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }
                    if($i <= 5){
                        echo '#top6';
                    }} ?>"><i class="fas fa-chevron-down"></i></a></div>
                <div class="top-mv-controller__up"><a href="<?php for($i = 3; $i >= 1; $i--){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }} ?>"><i class="fas fa-chevron-up"></i></a></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(get_theme_mod( 'cta5__view')): ?>
    <div class="top-mv top-mv__background--4" id="top4">
        <div class="inner">
            <div class="top-mv-container fadeIn">
                <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta5__title');?></h2>
                <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta5__text');?></p>
                <a class="top-mv-container__link" href="?php echo get_theme_mod( 'cta5__link-url');?>"><?php echo get_theme_mod( 'cta5__link-text');?></a>
            </div>
            <div class="top-mv-controller">
                <div class="top-mv-controller__down"><a href="<?php for($i = 5; $i <= 5; $i++){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }
                    if($i <= 5){
                        echo '#top6';
                    }} ?>"><i class="fas fa-chevron-down"></i></a></div>
                <div class="top-mv-controller__up"><a href="<?php for($i = 4; $i >= 1; $i--){
                    if(get_theme_mod( 'cta'.$i.'__view')){
                        echo '#top'.$i;
                        break;
                    }} ?>"><i class="fas fa-chevron-up"></i></a></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- /メインビジュアル -->
<main class="front-page">
    <div  class="top-blog-container" id="top6">
        <div class="inner">
            <h2 class="top-blog-container__title">NEW BLOG</h2>
            <div class="article-list">
            <?php query_posts('posts_per_page=6'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article class="article-list-item">
                    <a class="article-list-item__link" href="<?php the_permalink(); ?>">
                    <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('thumbnail'); ?>
                    <?php else : ?>
                        <img src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="NoImage" />
                    <?php endif ; ?>
                        <h2 class="article-list-item__title">
                            <?php the_title(); ?>
                        </h2>
                        <div class="article-list-item__info">
                            <p lass="">
                                <span class="article-list-item__category"><?php
                                $category = get_the_category();
                                $cat_name = $category[0]->cat_name;echo $cat_name; ?></span> | <span class="article-list-item__time"><?php the_time('Y.m.d'); ?></span>
                            </p>
                        </div>
                    </a>
                </article>
            <?php endwhile; endif; ?> 
            </div>
            <p class="top-blog-container__link right"><a href="/blog/">ブログ一覧<i class="fas fa-angle-double-right"></i></a></p>
        </div>
    </div>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
