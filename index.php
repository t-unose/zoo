
<?php get_header(); ?>
<!-- メインビジュアル -->
        <!-- トップ -->
        <?php if ( is_home() || is_front_page() ) : ?>
        <div class="news-bar">
            <div class="news-bar-container1">
                <ul class="news-bar-container1__list">
                    <li class="news-bar-container1__item"><time class="news-bar-container1__time">2019.2.10</time><span class="news-bar-container1__title">ニュースタイトル</span></li>
                    <!-- <li class="news-bar-container1__item"><time class="news-bar-container1__time">2019.1.1</time><span class="news-bar-container1__title">ニュースタイトル</span></li>
                    <li class="news-bar-container1__item"><time class="news-bar-container1__time">2018.12.1</time><span class="news-bar-container1__title">ニュースタイトル</span></li> -->
                </ul>
            </div>
            <div class="news-bar-container2">
                <a href="<?php  home_url(); ?>/news">NEWS一覧　<i class="fas fa-chevron-right"></i></a>
                </div>
        </div>
        <?php
        if(get_theme_mod( 'cta1__view')): ?>
        <div class="top-mv top-mv__background--1" id="top1">
            <div class="inner">
                <div class="top-mv-container">
                    <h1 class="top-mv-container__title"><?php echo get_theme_mod( 'cta1__title');?></h1>
                    <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta1__text');?></p>
                    <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta1__link-url');?>"><?php echo get_theme_mod( 'cta1__link-text');?></a>
                </div>
                <div class="top-mv-controller">
                    <div class="top-mv-controller__down"><a href="#top2"><i class="fas fa-chevron-down"></i></a></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php 
        if(get_theme_mod( 'cta2__view')): ?>
        <div class="top-mv top-mv__background--2" id="top2">
            <div class="inner">
                <div class="top-mv-container">
                    <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta2__title');?></h2>
                    <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta2__text');?></p>
                    <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta2__link-url');?>"><?php echo get_theme_mod( 'cta2__link-text');?></a>
                </div>
                <div class="top-mv-controller">
                    <div class="top-mv-controller__down"><a href="#top3"><i class="fas fa-chevron-down"></i></a></div>
                    <div class="top-mv-controller__up"><a href="#top1"><i class="fas fa-chevron-up"></i></a></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php 
        if(get_theme_mod( 'cta3__view')): ?>
        <div class="top-mv top-mv__background--3" id="top3">
            <div class="inner">
                <div class="top-mv-container">
                    <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta3__title');?></h2>
                    <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta3__text');?></p>
                    <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta3__link-url');?>"><?php echo get_theme_mod( 'cta3__link-text');?></a>
                </div>
                <div class="top-mv-controller">
                    <div class="top-mv-controller__down"><a href="#top4"><i class="fas fa-chevron-down"></i></a></div>
                    <div class="top-mv-controller__up"><a href="#top2"><i class="fas fa-chevron-up"></i></a></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php 
        if(get_theme_mod( 'cta4__view')): ?>
        <div class="top-mv top-mv__background--4" id="top4">
            <div class="inner">
                <div class="top-mv-container">
                    <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta4__title');?></h2>
                    <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta4__text');?></p>
                    <a class="top-mv-container__link" href="<?php echo get_theme_mod( 'cta4__link-url');?>"><?php echo get_theme_mod( 'cta4__link-text');?></a>
                </div>
                <div class="top-mv-controller">
                    <div class="top-mv-controller__down"><a href="#top5"><i class="fas fa-chevron-down"></i></a></div>
                    <div class="top-mv-controller__up"><a href="#top3"><i class="fas fa-chevron-up"></i></a></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php 
        if(get_theme_mod( 'cta5__view')): ?>
        <div class="top-mv top-mv__background--4" id="top4">
            <div class="inner">
                <div class="top-mv-container">
                    <h2 class="top-mv-container__title"><?php echo get_theme_mod( 'cta5__title');?></h2>
                    <p class="top-mv-container__text"><?php echo get_theme_mod( 'cta5__text');?></p>
                    <a class="top-mv-container__link" href="?php echo get_theme_mod( 'cta5__link-url');?>"><?php echo get_theme_mod( 'cta5__link-text');?></a>
                </div>
                <div class="top-mv-controller">
                    <div class="top-mv-controller__down"><a href="#top5"><i class="fas fa-chevron-down"></i></a></div>
                    <div class="top-mv-controller__up"><a href="#top3"><i class="fas fa-chevron-up"></i></a></div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!-- アーカイブ -->
        <?php elseif ( is_archive() ) : ?>
        <div class="mv">
            <div class="mv-translucent">
                <div class="inner">
                    <h1 class="mv-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <!-- 記事ページ -->
        <?php elseif ( is_single() ) : ?>
        <div class="mv">
            <div class="mv-translucent">
                <div class="inner">
                    <h1 class="mv-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <!-- 固定ページ -->
        <?php elseif( is_page() ) : ?>
        <div class="mv">
            <div class="mv-translucent">
                <div class="inner">
                    <h1 class="mv-title"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <?php else : ?>
        <?php endif; ?>
    <!-- /メインビジュアル -->
    <!-- パンくず -->
    <?php if ( !is_home() && !is_front_page() ) : ?>
        <div class="inner">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
            } ?>
        </div>
    <?php endif; ?>
    <!-- /パンくず -->
    
    <main class="">
        <!-- トップ -->
        <?php if ( is_home() || is_front_page() ) : ?>
            <div  class="top-blog-container" id="top5">
                <div class="inner">
                    <h2 class="top-blog-container__title">NEW BLOG</h2>
                    <div class="article-list">
                    <?php query_posts('posts_per_page=6'); ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article class="article-list-item">
                            <a class="article-list-item__link" href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            <?php else : ?>
                                <img src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="NoImage" />
                            <?php endif ; ?>
                                <h2 class="article-list-item__title">
                                    <?php the_title(); ?>
                                </h2>
                                <div class="article-list-item__info">
                                    <p lass="">
                                        <span class="article-list-item__category"><?php
                                        $category = get_the_category();
                                        $cat_name = $category[0]->cat_name;echo $cat_name; ?></span> | <span class="article-list-item__time"><?php the_time('Y.m.d'); ?></span></p>
                                </div>
                            </a>
                        </article>
                    <?php endwhile; endif; ?> 
                    </div>
                    <p class="top-blog-container__link right"><a href="/blog/">ブログ一覧<i class="fas fa-angle-double-right"></i></a></p>
                </div>
            </div>





        <!-- アーカイブ -->
        <?php elseif ( is_archive() ) : ?>
        <div class="inner">
        <div class="article-list">
            <?php
                if ( have_posts() ) {
                while ( have_posts() ){
                    the_post();
                    ?>
                    <article class="article-list-item">
                        <a class="article-list-item__link" href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            <?php else : ?>
                                <img class="article-list-item__thumbnail" src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="デフォルト画像" />
                            <?php endif ; ?>
                            <h2 class="article-list-item__title">
                            <?php the_title(); ?>
                            </h2>
                            <div class="article-list-item__info">
                                <p class="">
                                    <span class="article-list-item__category"><?php
                                    $category = get_the_category();
                                    $cat_name = $category[0]->cat_name;echo $cat_name; ?></span> | <span class="article-list-item__time"><?php the_time('Y.m.d'); ?></span></p>
                            </div>
                        </a>
                    </article>
                    <?php
                }
            }
            ?>
        </div>
        <?php
                //ページネーション実装
                $big = 9999999999;
                $arg = array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'current' => max( 1, get_query_var('paged') ),
                    'total'   => $wp_query->max_num_pages,
                    'prev_text'    => '<i class="fas fa-angle-double-left"></i>前へ',
                    'next_text'    => '次へ<i class="fas fa-angle-double-right"></i>',
                    'type'    => 'list'

                );
                echo paginate_links($arg);
            ?>
        </div>




        <!-- 記事ページ -->
        <?php elseif ( is_single() ) : ?>
            <div>
                <div class="inner">
                    <div><title class=""><?php wp_title(); ?></title></div>
                    <div><time class=""><?php the_time('Y.m.d'); ?></time></div>
                    <div class=""><?php the_category(); ?></div>
                    <div class=""><?php the_author(); ?></div>
                    <div><?php the_post_thumbnail();?></div>
                    <div><?php the_content(); ?></div>
                    <div class="">
                        <?php if (get_next_post()):?>
                            <div class="next"><?php next_post_link('%link','次の記事へ　%title',TRUE); ?></div>
                        <?php endif; ?>
                        <?php if (get_previous_post()):?>
                            <div class="prev"><?php previous_post_link('%link','前の記事へ　%title',TRUE); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="inner">
                    <h2>関連記事</h2>
                    <!-- related-list -->
                    <div class="related-list">
                        <div class="related-list__display">
                        <?php
                            // カテゴリーが複数設定されている場合は、どれかをランダムに取得
                            $categories = wp_get_post_categories($post->ID, array('orderby'=>'rand'));
                            //表示したい記事要素を設定
                            if ($categories) {
                                $args = array(
                                    'category__in' => array($categories[0]), // カテゴリーのIDで記事を取得
                                    'post__not_in' => array($post->ID), // 表示している記事は除外する
                                    'showposts'=>12, // 取得したい記事数
                                    'caller_get_posts'=>1, // 取得した記事を1番目から表示する
                                    'orderby'=> 'rand' // ランダムで取得する
                                ); 
                            $my_query = new WP_Query($args);
                            if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) { $my_query->the_post();
                            ?>
                                <div class="related-article">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <div class="">
                                        <?php if (has_post_thumbnail()) : ?>
                                            <?php the_post_thumbnail('thumbnail'); ?>
                                        <?php else : ?>
                                            <img class="related-article__thumbnail" src="<?php bloginfo('template_url'); ?>/img/noimage.png" alt="デフォルト画像" />
                                        <?php endif ; ?>
                                            <h3 class="related-article__title"><?php the_title(); ?></h3>
                                            <p class="related-article__time"><?php
                                                $category = get_the_category();
                                                $cat_name = $category[0]->cat_name;echo $cat_name; ?> | <?php the_time('Y.m.d'); ?></p>
                                        </div>
                                    </a>
                                </div>
                            <?php } wp_reset_query();
                            } else { ?>
                                <p class="">関連記事がない</p>
                        <?php } } ?>
                        </div>
                        <div class="related-list__next">次へ</div>
                        <div class="related-list__prev">戻る</div>
                    </div>
                    <!-- /related-list -->
                </div>
            </div>
        <!-- 固定ページ -->
        <?php elseif( is_page() ) : ?>
        <div class="inner">
        <div><?php the_content(); ?></div>
        </div>
        <?php else : ?>

        <?php endif; ?>
    </main>


    <?php get_sidebar(); ?>
    <?php get_footer(); ?>