<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

//カスタマイザーファイルをインクルード
require_once locate_template('customizer.php');



//メニューを追加
register_nav_menu('header_menu', 'グローバルナビ');
register_nav_menu('footer-sitemaps_menu1', 'フッターサイトマップ1');
register_nav_menu('footer-sitemaps_menu2', 'フッターサイトマップ2');
register_nav_menu('footer-sitemaps_menu3', 'フッターサイトマップ3');
register_nav_menu('footer-sitemaps_menu4', 'フッターサイトマップ4');

// アイキャッチ画像を有効にする。
add_theme_support('post-thumbnails');

//投稿ページを追加
add_filter( 'register_post_type_args', function( $args, $post_type ) {
    global $wp_rewrite;
    if ( 'post' === $post_type ) {
        $archive_slug = 'blog';
        // Setting 'has_archive' ensures get_post_type_archive_template() returns an archive.php template.
        $args['has_archive'] = $archive_slug;
        // We have to register rewrite rules, because WordPress won't do it for us unless $args['rewrite'] is true.
        $archive_slug = $wp_rewrite->root . $archive_slug;
        add_rewrite_rule( "{$archive_slug}/?$", "index.php?post_type=$post_type", 'top' );
        $feeds = '(' . trim( implode( '|', $wp_rewrite->feeds ) ) . ')';
        add_rewrite_rule( "{$archive_slug}/feed/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
        add_rewrite_rule( "{$archive_slug}/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
        add_rewrite_rule( "{$archive_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type=$post_type" . '&paged=$matches[1]', 'top' );
    }
    return $args;
}, 10, 2 );


//ページネーション
function pagination($pages = '', $range = 1){
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages){
            $pages = 1;
        }
    }
    if(1 != $pages){
        echo "<div class=\"pagination center\"><div class=\"pagination-box\">";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."' class=\"pagination__first\">&laquo;</a>";
        if($paged > 1 && $showitems < $pages && !wp_is_mobile()) echo "<a href='".get_pagenum_link($paged - 1)."' class=\"pagination__prev\">&lsaquo;</a>";
        
        for ($i=1; $i <= $pages; $i++){
            if(!wp_is_mobile()){
                if (1 != $pages &&( !($i >= $paged+$range+3 || $i <= $paged-$range-3) || $pages <= $showitems )){
                    echo ($paged == $i)? "<span class=\"pagination__curren\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"pagination__inactive\">".$i."</a>";
                }
            }else{
                if (1 != $pages &&( !($i >= $paged+$range+2 || $i <= $paged-$range-2) || $pages <= $showitems )){
                    echo ($paged == $i)? "<span class=\"pagination__curren\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"pagination__inactive\">".$i."</a>";
                }
            }
        }

        if ($paged < $pages && $showitems < $pages && !wp_is_mobile()) echo "<a href=\"".get_pagenum_link($paged + 1)."\" class=\"pagination__next\">&rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."' class=\"pagination__last\">&raquo;</a>";
        echo "</div></div>\n";
    }
}








//==============================================================================
//
//	「記事、固定ページ」固有のCSS,JavaScriptを記述できるカスタムフィールドを作成する
//
//==============================================================================
//--------------------------------------------------------------
//	アクションフックに登録：管理画面にカスタムボックスをエントリー
//--------------------------------------------------------------
        add_action(
            'add_meta_boxes', 
            function(){
                $screens = array('post', 'page');	//投稿ページと固定ページに表示
                foreach($screens as $scrn){
                    add_meta_box(
                        'peralab-custombox-css-internal', 	//編集画面セクションのHTML ID
                        'この記事限定のCSS, JavaScript', 	//メタボックスのタイトル
                        'PeralabCssInternal_CustomBoxCreate', 	//入力フォーム作成で呼び出されるコールバック
                        $scrn, 								//表示するページ
                        'normal', 							//メタボックス表示箇所(advanced, normal, side)
                        'default', 							//表示優先度(high, core, default, low)
                        null);								//コールバック時に渡す引数があれば指定
                }
            }
        );

        //--------------------------------------------------------------
        //	メタボックスを作成
        //--------------------------------------------------------------
        function PeralabCssInternal_CustomBoxCreate($post){	//$postには現在の投稿記事データが入っています
            //入力済みのデータを取得
            $data_str = get_post_meta($post->ID, "metakey_css", true);
            $jsext_str = get_post_meta($post->ID, "metakey_jsext", true);
            $jshead_str = get_post_meta($post->ID, "metakey_jshead", true);
            $jsbody_str = get_post_meta($post->ID, "metakey_jsbody", true);
            //初回の場合、データが入っていないので空チェック
            //get_post_metaの第３引数がtrue指定の場合、データが無い時は空文字列になっています
            //このウィジェットは空文字列のままで問題ないので特にチェックはしません。
            //if($data_str == ''){}
            
            //nonce作成
            wp_nonce_field('action-noncekey-css-internal', 'noncename-css-internal');

            ?>
            <div>
            
            <p><label>CSS, JavaScript外部参照<br />　例）&lt;link href=&quot;//domain/wp-content/css/mystyle.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; /&gt;<br />　　　&lt;script src=&quot;//domain/wp-content/js/myscript.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;<br />
            <textarea id="id-metabox_jsext_textarea" name="name-metabox_jsext_textarea" rows="2" cols="30" style="width:100%;"><?php echo esc_attr($jsext_str); ?></textarea>
            </label></p>

            <p><label>CSS内部参照（head要素内に出力）<br />
            <textarea id="id-metabox_css_textarea" name="name-metabox_css_textarea" rows="5" cols="30" style="width:100%;"><?php echo esc_attr($data_str); ?></textarea>
            </label></p>

            <p><label>JavaScript内部参照（head要素内に出力）<br />
            <textarea id="id-metabox_jshead_textarea" name="name-metabox_jshead_textarea" rows="5" cols="30" style="width:100%;"><?php echo esc_attr($jshead_str); ?></textarea>
            </label></p>

            <p><label>JavaScript内部参照（/bodyタグの真上に出力）<br />
            <textarea id="id-metabox_jsbody_textarea" name="name-metabox_jsbody_textarea" rows="5" cols="30" style="width:100%;"><?php echo esc_attr($jsbody_str); ?></textarea>
            </label></p>
            
            </div>
            <?php
        }

        //--------------------------------------------------------------
        //	カスタムボックス内のフィールド値更新処理
        //--------------------------------------------------------------
        add_action(
            'save_post', 
            function($post_id){
                //nonceを確認
                if(isset($_POST['noncename-css-internal']) == false 
                        || wp_verify_nonce($_POST['noncename-css-internal'], 'action-noncekey-css-internal') == false) {
                    return;	//nonceを認証できなかった
                }
                
                //自動保存ルーチンかどうかチェック。そうだった場合はフォームを送信しない(何もしない)
                if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
                    return;
                }
                
                //パーミッション確認
                if(isset($_POST['post_type'])){
                    if($_POST['post_type'] == 'page'){
                        if(!current_user_can('edit_page', $post_id)){
                            return;	//固定ページを編集する権限がない
                        }
                    }
                    else{
                        if(!current_user_can('edit_post', $post_id)){
                            return;	//記事を編集する権限がない
                        }
                    }
                }
                
                //== 確認ここまで ==

                
                //予約投稿時は、データが有るにも関わらず$_POSTからデータ取得ができないので、
                //issetでデータ確認が出来るときのみ値の更新処理を行います。
                if(isset($_POST['name-metabox_css_textarea'])){
                    update_post_meta($post_id, "metakey_css", $_POST['name-metabox_css_textarea']);
                }
                if(isset($_POST['name-metabox_jsext_textarea'])){
                    update_post_meta($post_id, "metakey_jsext", $_POST['name-metabox_jsext_textarea']);
                }
                if(isset($_POST['name-metabox_jshead_textarea'])){
                    update_post_meta($post_id, "metakey_jshead", $_POST['name-metabox_jshead_textarea']);
                }
                if(isset($_POST['name-metabox_jsbody_textarea'])){
                    update_post_meta($post_id, "metakey_jsbody", $_POST['name-metabox_jsbody_textarea']);
                }
            }
        );

        //--------------------------------------------------------------
        //	出力用のアクションフックを登録
        //--------------------------------------------------------------
        add_action(
            'wp_head',
            function(){
                if(is_single() == false && is_page() == false){
                    return;	//投稿記事でも固定ページでもないので何もせずに終了
                }
                
                $data_str = get_post_meta(get_the_ID(), "metakey_css", true);
                if($data_str != '' && ctype_space($data_str) == false){	//空文字チェック
                    echo '<style type="text/css">' . PHP_EOL . $data_str . PHP_EOL . '</style>' . PHP_EOL;
                }
                
                $data_str = get_post_meta(get_the_ID(), "metakey_jsext", true);
                if($data_str != '' && ctype_space($data_str) == false){	//空文字チェック
                    echo $data_str . PHP_EOL;
                }
                
                $data_str = get_post_meta(get_the_ID(), "metakey_jshead", true);
                if($data_str != '' && ctype_space($data_str) == false){	//空文字チェック
                    echo '<script type="text/javascript">' . PHP_EOL . $data_str . PHP_EOL . '</script>' . PHP_EOL;
                }
            },
            1000	//実行プライオリティ。テーマデフォルトのCSSに上書きされないように後ろで実行する
        );
        add_action(
            'wp_footer',
            function(){
                if(is_single() == false && is_page() == false){
                    return;	//投稿記事でも固定ページでもないので何もせずに終了
                }
                
                $data_str = get_post_meta(get_the_ID(), "metakey_jsbody", true);
                if($data_str != '' && ctype_space($data_str) == false){	//空文字チェック
                    echo '<script type="text/javascript">' . PHP_EOL . $data_str . PHP_EOL . '</script>' . PHP_EOL;
                }
            },
            1000	//実行プライオリティ
        );