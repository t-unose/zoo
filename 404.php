<?php get_header(); ?>
<!-- メインビジュアル -->
<div class="mv">
    <div class="mv-translucent">
        <div class="inner">
            <h1 class="mv-title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<!-- /メインビジュアル -->
<!-- パンくず -->
<div class="inner-breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
</div>
<!-- /パンくず -->
<main class="404-page">
    <div class="inner">
        <p>ページが見つかりません</p>
    </div>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>