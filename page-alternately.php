<?php 
/**
 * Template Name: page-alternately
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header(); ?>
<!-- メインビジュアル -->
<div class="mv">
    <div class="mv-translucent">
        <div class="inner">
            <h1 class="mv-title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<!-- /メインビジュアル -->
<!-- パンくず -->
<div class="inner-breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
</div>
<!-- /パンくず -->

<main class="fixed-page fixed-page-alternately">
    <div class="inner">
        <div class="fixed-post fixed-post-alternately">
            <div class="fixed-post__content fixed-post-alternately__content"><?php the_content(); ?></div>
        </div>
    </div>
</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>