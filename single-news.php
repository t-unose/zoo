<?php get_header(); ?>
<!-- メインビジュアル -->
<div class="mv">
    <div class="mv-translucent">
        <div class="inner">
            <h1 class="mv-title"><?php the_title(); ?></h1>
        </div>
    </div>
</div>
<!-- /メインビジュアル -->
<!-- パンくず -->
<div class="inner-breadcrumbs">
        <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    } ?>
</div>
<!-- /パンくず -->
<main class="single-page">
    <!-- 記事ページ -->
    <div>
        <div class="inner">
            <div class="single-post">
                <div class="single-post__title"><?php the_title(); ?></div>
                <div class="single-post__info">
                    <time class="single-post__time"><i class="fas fa-clock"></i><?php the_time('Y.m.d'); ?></time>
                </div>

                <div class="single-post__content"><?php the_content(); ?></div>

                <?php // 現在の投稿に隣接している前後の投稿を取得する
                $prev_post = get_previous_post(); // 前の投稿を取得
                $next_post = get_next_post(); // 次の投稿を取得
                if( $prev_post || $next_post ): // どちらか一方があれば表示
                ?>
                    <div class="single-post-link">
                        <div class="single-post-link__next">
                        <?php if (get_next_post()): ?>
                            <a class="single-post-link__next-box" href="<?php echo get_permalink( $next_post->ID ); ?>"><p class="single-post-link__title"><?php echo $next_post->post_title ?></p></a>
                        <?php endif; ?>
                        </div>
                        <div class="single-post-link__prev">
                        <?php if (get_previous_post()):?>
                        <?php $next_post= $next_post->ID; ?>
                            <a class="single-post-link__prev-box" href="<?php echo get_permalink( $prev_post->ID ); ?>"><p class="single-post-link__title"><?php echo $prev_post->post_title ?></p></a>
                        <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
