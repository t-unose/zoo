<?php
/*
Template Name: テストテンプレート
Template Post Type: post,news
*/
?>
    <?php get_header(); ?>

    <main>
        <?php the_content(); ?>
    </main>


    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
